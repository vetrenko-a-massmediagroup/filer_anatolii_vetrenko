@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('List') }}</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                    <div>
                        Files count {{$filesCount}}
                    </div>

                    @empty($files)
                        <div>No files uploaded.</div>
                    @else
                        <table class="table">
                            <thead>
                                <tr>
                                    <th scope="col">{{__('ID')}}</th>
                                    <th scope="col">{{__('Name')}}</th>
                                </tr>
                            </thead>
                            <tbody>
                            @foreach($files as $file)
                                <tr>
                                    <td scope="row">
                                        <a href="{{route('files.show', ['file' => $file['id']])}}">
                                            {{$file['id']}}
                                        </a>
                                    </td>
                                    <td>{{$file['name']}}</td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    @endempty
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
