@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('File') }}</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                    @isset($file['delete_at'])
                        <div>Will be deleted: {{$file['delete_at']}}</div>
                    @endisset

                    @isset($file['comment'])
                        <div>Comment: {{$file['comment']}}</div>
                    @endisset

                    <form class="mt-2" action="{{route('files.destroy', ['file' => $file['id']])}}" method="post">
                        @method('DELETE')
                        @csrf

                        <button class="btn btn-danger" type="submit">{{__('Delete file')}}</button>
                    </form>

                    <form class="mt-2 form-inline" action="{{route('share.store')}}" method="post">
                        @csrf
                        <input type="hidden" name="file_id" value="{{$file['id']}}">
                        <div class="form-group">
                            <label for="type">{{__('Token type:')}} </label>
                            <select name="type" class="form-control ml-3" id="type">
                                <option value="0" selected>{{__('Unlimited')}}</option>
                                <option value="1">{{__('Limited')}}</option>
                            </select>
                        </div>

                        <button class="btn btn-success ml-3" type="submit">{{__('Generate token')}}</button>
                    </form>

                    <table class="table mt-3">
                        <thead>
                            <tr>
                                <th scope="col">{{__('Share link')}}</th>
                                <th scope="col">{{__('Active')}}</th>
                                <th scope="col">{{__('Type')}}</th>
                                <th scope="col">{{__('Viewed')}}</th>
                            </tr>
                        </thead>
                        <tbody>
                        @foreach($file['share_tokens'] ?? [] as $token)
                            <tr>
                                <td scope="row">
                                    <a href="{{route('share.get', ['token' => $token['token']])}}" target="_blank">
                                        {{$token['token']}}
                                    </a>
                                </td>
                                <td>{{$token['active'] ? '+' : '-'}}</td>
                                <td>
                                    @if($token['type'] === 0)
                                        Unlimited
                                    @elseif($token['type'] === 1)
                                        Limited
                                    @endif
                                </td>
                                <td>{{$token['viewed']}}</td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
