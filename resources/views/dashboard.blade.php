@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('Dashboard') }}</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                    <form action="{{route('files.store')}}" method="post" enctype="multipart/form-data">
                        @csrf
                        <div class="form-group">
                            <small id="fileHelp" class="form-text text-muted">{{__('Allowed only images up to 5Mb')}}</small>
                            <input name="file" type="file"
                                   class="file-input @error('file') is-invalid @enderror" id="file"
                                   aria-describedby="fileHelp"
                                   accept=".jpg, .jpeg, .png, .bmp">

                            @error('file')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror

                        </div>

                        <div class="form-group">
                            <label for="comment">{{__('Comment:')}}</label>
                            <input name="comment" type="text" class="form-control @error('comment') is-invalid @enderror" id="comment"
                                   aria-describedby="commentHelp" value="{{old('comment')}}">

                            @error('comment')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>

                        <div class="form-group">
                            <label for="date">{{__('Delete file at:')}}</label>
                            <input name="delete_at" type="text" class="form-control col-3 @error('delete_at') is-invalid @enderror" id="date"
                                   aria-describedby="dateHelp" value="{{old('delete_at')}}">
                            @error('delete_at')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>

                        <button class="btn btn-primary" type="submit">{{__('Upload')}}</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
