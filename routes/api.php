<?php

use App\Http\Controllers\API\v1\FileController;
use App\Http\Controllers\API\v1\AuthController;
use App\Http\Controllers\API\v1\ShareController;
use Illuminate\Support\Facades\Route;

Route::group(['prefix' => 'v1', 'as' => 'api.'], function () {
    Route::post('login', [AuthController::class, 'login'])->name('login');

    Route::middleware('auth:sanctum')->group(function () {
        Route::post('logout', [AuthController::class, 'logout'])->name('logout');

        Route::apiResource('files', FileController::class)->except('update');
        Route::get('files/{file}/download', [FileController::class, 'download'])->name('files.download');

        Route::post('share', [ShareController::class, 'store'])->name('share.store');
    });
});
