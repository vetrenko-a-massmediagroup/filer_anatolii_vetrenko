<?php

use App\Http\Controllers\DashboardController;
use App\Http\Controllers\FileController;
use App\Http\Controllers\ReportController;
use App\Http\Controllers\ShareController;
use Illuminate\Support\Facades\Route;

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::middleware('auth')->group(function () {
    Route::get('dashboard', DashboardController::class)->name('dashboard');

    Route::resource('files', FileController::class)->except(['edit', 'create', 'update']);

    Route::post('share', [ShareController::class, 'store'])->name('share.store');

    Route::get('reports', [ReportController::class, 'index'])->name('reports.index');
});

Route::get('share/{token}', [ShareController::class, 'get'])->name('share.get');
