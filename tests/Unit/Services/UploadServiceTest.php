<?php

declare(strict_types=1);

namespace Tests\Unit\Services;

use App\Services\UploadService;
use Illuminate\Support\Facades\Storage;
use Illuminate\Http\UploadedFile;
use Tests\TestCase;

class UploadServiceTest extends TestCase
{
    public function testUpload()
    {
        Storage::fake('test');

        $uploadedFile = UploadedFile::fake()->image('photo.png');

        $uploader = new UploadService();
        $result = $uploader->upload($uploadedFile, '/', 'test');

        Storage::disk('test')->assertExists($result);
    }
}
