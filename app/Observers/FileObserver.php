<?php

namespace App\Observers;

use App\Models\File;

class FileObserver
{
    /**
     * Handle events after all transactions are committed.
     *
     * @var bool
     */
    public $afterCommit = true;

    /**
     * Handle the File "deleted" event.
     *
     * @param  \App\Models\File  $file
     * @return void
     */
    public function deleted(File $file): void
    {
        $file->shareTokens()->delete();
    }

    /**
     * Handle the File "restored" event.
     *
     * @param  \App\Models\File  $file
     * @return void
     */
    public function restored(File $file): void
    {
        $file->shareTokens()->withTrashed()->restore();
    }
}
