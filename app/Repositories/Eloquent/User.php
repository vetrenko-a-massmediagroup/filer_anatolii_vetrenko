<?php

declare(strict_types=1);

namespace App\Repositories\Eloquent;

use App\Repositories\Contracts\UserRepository;

class User extends BaseRepository implements UserRepository
{
    public function model(): string
    {
        return \App\Models\User::class;
    }
}
