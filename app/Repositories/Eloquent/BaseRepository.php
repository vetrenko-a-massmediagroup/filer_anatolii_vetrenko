<?php

declare(strict_types=1);

namespace App\Repositories\Eloquent;

use App\Repositories\Contracts\Repository;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Container\Container as Application;

abstract class BaseRepository implements Repository
{
    protected Model $model;
    private Application $app;

    public function __construct(Application $app)
    {
        $this->app = $app;
        $this->makeModel();
    }

    abstract public function model(): string;

    private function makeModel()
    {
        $model = $this->app->make($this->model());

        if (!$model instanceof Model) {
            throw new \Exception("Class {$this->model()} must be an instance of Illuminate\\Database\\Eloquent\\Model");
        }

        return $this->model = $model;
    }

    public function all(): array
    {
        return $this->model::all()->toArray();
    }

    public function find(int $id): array
    {
        return $this->model::findOrFail($id)->toArray();
    }

    public function create(array $attributes): array
    {
        return $this->model::create($attributes)->toArray();
    }

    public function delete(int $id): void
    {
        $model = $this->model::findOrFail($id);

        $model->delete();
    }

    public function latest(): array
    {
        return $this->model::latest()->get()->toArray();
    }

}
