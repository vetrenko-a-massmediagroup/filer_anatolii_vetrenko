<?php

declare(strict_types=1);

namespace App\Repositories\Eloquent;

use App\Repositories\Contracts\ShareTokenRepository;

class ShareToken extends BaseRepository implements ShareTokenRepository
{
    public function model(): string
    {
        return \App\Models\ShareToken::class;
    }

    public function getActiveShareTokenWithFile(string $token): array
    {
        return $this->model::with('file')
            ->where('token', $token)
            ->active()
            ->firstOrFail()
            ->toArray();
    }

    public function incrementViewed(int $id): int
    {
        $shareToken = $this->model::findOrFail($id);

        return $shareToken->increment('viewed');
    }

    public function deactivate(int $id): void
    {
        $shareToken = $this->model::findOrFail($id);
        $shareToken->active = $this->model()::INACTIVE;
        $shareToken->save();
    }
}
