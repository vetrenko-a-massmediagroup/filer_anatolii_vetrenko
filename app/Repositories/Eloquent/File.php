<?php

declare(strict_types=1);

namespace App\Repositories\Eloquent;

use App\Repositories\Contracts\FileRepository;

class File extends BaseRepository implements FileRepository
{
    public function model(): string
    {
        return \App\Models\File::class;
    }

    public function getByIdWithShareTokens(int $id): array
    {
        return $this->model::with('shareTokens')->findOrFail($id)->toArray();
    }
}
