<?php

declare(strict_types=1);

namespace App\Repositories\Contracts;

interface Repository
{
    public function all(): array;

    public function find(int $id): array;

    public function create(array $attributes): array;

    public function delete(int $id);

    public function latest(): array;
}
