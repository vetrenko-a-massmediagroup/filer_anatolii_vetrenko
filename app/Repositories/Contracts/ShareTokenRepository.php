<?php

declare(strict_types=1);

namespace App\Repositories\Contracts;

interface ShareTokenRepository extends Repository
{
    public function getActiveShareTokenWithFile(string $token): array;

    public function incrementViewed(int $id): int;

    public function deactivate(int $id): void;
}
