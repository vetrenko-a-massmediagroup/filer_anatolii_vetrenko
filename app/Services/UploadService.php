<?php

declare(strict_types=1);

namespace App\Services;

use Illuminate\Http\UploadedFile;

class UploadService
{
    /**
     * @param  UploadedFile  $file
     * @param  string  $path
     * @param  string  $disk
     * @return false|string
     */
    public function upload(UploadedFile $file, string $path, string $disk = 'local')
    {
        return $file->store($path, $disk);
    }
}
