<?php

declare(strict_types=1);

namespace App\Services;

use App\Repositories\Contracts\FileRepository;

class FileService
{
    private UploadService $uploadService;
    private FileRepository $repository;

    public function __construct(UploadService $uploadService, FileRepository $repository)
    {
        $this->uploadService = $uploadService;
        $this->repository = $repository;
    }

    public function index(): array
    {
        $files = $this->repository->latest();
        $filesCount = count($files);

        return compact('files', 'filesCount');
    }

    public function store(array $input, int $userId): ?array
    {
        $path = 'upload/images/' . $userId;

        $filePath = $this->uploadService->upload($input['file'], $path);

        if (!$filePath) {
            return null;
        }

        if (isset($input['delete_at'])) {
            $input['delete_at'] = date('Y-m-d H:i:s', strtotime($input['delete_at']));
        }

        return $this->repository->create(array_merge([
            'user_id' => $userId,
            'path' => $filePath,
            'name' => $input['file']->getClientOriginalName()
        ], $input));
    }

    public function show(int $id): array
    {
        return $this->repository->getByIdWithShareTokens($id);
    }

    public function destroy(int $id): void
    {
        $this->repository->delete($id);
    }
}
