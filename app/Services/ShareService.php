<?php

declare(strict_types=1);

namespace App\Services;

use App\Models\ShareToken;
use App\Repositories\Contracts\ShareTokenRepository;

class ShareService
{
    private ShareTokenRepository $repository;

    public function __construct(ShareTokenRepository $repository)
    {
        $this->repository = $repository;
    }

    public function store(array $input): array
    {
        return $this->repository->create($input);
    }

    public function getFilePathByToken(string $token): string
    {
        $shareToken = $this->repository->getActiveShareTokenWithFile($token);

        $shareToken['viewed'] = $this->repository->incrementViewed($shareToken['id']);

        if (
            $shareToken['type'] === ShareToken::TYPE_LIMITED
            && $shareToken['viewed'] >= 1
        ) {
            $this->repository->deactivate($shareToken['id']);
        }

        return $shareToken['file']['path'];
    }
}
