<?php

declare(strict_types=1);

namespace App\Services;

use App\Models\File;
use App\Models\ShareToken;

class ReportService
{
    public function filesViewCount(int $userId): int
    {
        return ShareToken::leftjoin('files', 'files.id', 'share_tokens.file_id')
            ->where('files.user_id', $userId)
            ->sum('viewed');
    }

    public function currentFilesCount(): int
    {
        return File::count();
    }

    public function removedFilesCount(): int
    {
        return File::onlyTrashed()->count();
    }

    public function usedLimitedLinks(int $userId): int
    {
        return ShareToken::leftjoin('files', 'files.id', 'share_tokens.file_id')
            ->where('files.user_id', $userId)
            ->limited()
            ->inactive()
            ->count();
    }

    public function limitedLinkCount(int $userId): int
    {
        return ShareToken::leftjoin('files', 'files.id', 'share_tokens.file_id')
            ->where('files.user_id', $userId)
            ->limited()
            ->count();
    }

    public function generalReport(int $userId): array
    {
        return [
            'currentFilesCount ' . $this->currentFilesCount(),
            'removedFilesCount ' . $this->removedFilesCount(),
            'usedLimitedLinks ' . $this->usedLimitedLinks($userId),
            'filesViewCount ' . $this->filesViewCount($userId),
            'limitedLinkCount ' . $this->limitedLinkCount($userId),
        ];
    }
}
