<?php

namespace App\Models;

use App\Scopes\AuthenticatedUserScope;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Str;

/**
 * @OA\Schema()
 */
class ShareToken extends Model
{
    use HasFactory;
    use SoftDeletes;

    public const INACTIVE = 0;
    public const ACTIVE = 1;

    public const TYPE_UNLIMITED = 0;
    public const TYPE_LIMITED = 1;

    public static $types = [
        self::TYPE_UNLIMITED => 'UNLIMITED',
        self::TYPE_LIMITED => 'LIMITED',
    ];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'file_id',
        'token',
        'type',
        'viewed',
    ];

    /**
     * Scope a query to only include active token.
     *
     * @param  Builder  $query
     * @return Builder
     */
    public function scopeActive($query)
    {
        return $query->where('active', self::ACTIVE);
    }

    /**
     * Scope a query to only include inactive token.
     *
     * @param  Builder  $query
     * @return Builder
     */
    public function scopeInactive($query)
    {
        return $query->where('active', self::INACTIVE);
    }

    /**
     * Scope a query to only include tokens of a given type.
     *
     * @param  Builder  $query
     * @param  mixed  $type
     * @return Builder
     */
    public function scopeLimited($query)
    {
        return $query->where('type', self::TYPE_LIMITED);
    }

    /**
     * Scope a query to only include tokens of a given type.
     *
     * @param  Builder  $query
     * @param  mixed  $type
     * @return Builder
     */
    public function scopeUnlimited($query)
    {
        return $query->where('type', self::TYPE_UNLIMITED);
    }

    public function file(): BelongsTo
    {
        return $this->belongsTo(File::class)->withoutGlobalScope(AuthenticatedUserScope::class);
    }

    /**
     * @param  array  $attributes
     * @return Builder|Model
     */
    public static function create(array $attributes = [])
    {
        $attributes['token'] = Str::random(20);

        return static::query()->create($attributes);
    }
}
