<?php

namespace App\Providers;

use App\Repositories\Contracts\FileRepository;
use App\Repositories\Contracts\ShareTokenRepository;
use App\Repositories\Contracts\UserRepository;
use Illuminate\Support\ServiceProvider;

class RepositoryServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->bind([
            ShareTokenRepository::class => \App\Repositories\Eloquent\ShareToken::class,
            UserRepository::class => \App\Repositories\Eloquent\User::class,
            FileRepository::class => \App\Repositories\Eloquent\File::class,
        ]);
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    private function bind($relations)
    {
        foreach ($relations as $interface => $realisation) {
            $this->app->bind($interface, $realisation);
        }
    }
}
