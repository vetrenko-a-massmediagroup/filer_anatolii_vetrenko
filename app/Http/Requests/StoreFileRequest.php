<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StoreFileRequest extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        return [
            'file' => [
                'required',
                'image',
                'max:5120',
            ],
            'comment' => [
                'nullable',
                'string',
                'max:65535',
            ],
            'delete_at' => [
                'nullable',
                'after:today',
                'date_format:Y-m-d',
            ],
        ];
    }

}
