<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StoreShareTokenRequest extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        return [
            'file_id' => [
                'required',
                'exists:files,id',
            ],
            'type' => [
                'integer',
                'between:0,1',
            ],
        ];
    }
}
