<?php

namespace App\Http\Controllers;

use App\Http\Requests\StoreShareTokenRequest;
use App\Services\ShareService;
use Illuminate\Http\RedirectResponse;
use Illuminate\Support\Facades\Storage;

class ShareController extends Controller
{
    private ShareService $service;

    public function __construct(ShareService $service)
    {
        $this->service = $service;
    }

    public function store(StoreShareTokenRequest $request): RedirectResponse
    {
        $this->service->store($request->validated());

        return redirect()->back();
    }

    /**
     * @return mixed
     */
    public function get(string $token)
    {
        $filePath = $this->service->getFilePathByToken($token);

        return Storage::response($filePath);
    }
}
