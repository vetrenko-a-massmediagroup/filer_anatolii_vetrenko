<?php

namespace App\Http\Controllers;

use App\Http\Requests\StoreFileRequest;
use App\Services\FileService;
use Illuminate\Http\RedirectResponse;
use Illuminate\Support\Facades\Auth;

class FileController extends Controller
{
    private FileService $service;

    public function __construct(FileService $service)
    {
        $this->service = $service;
    }

    /**
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function index()
    {
        $result = $this->service->index();

        return view('pages.files.index')->with($result);
    }

    public function store(StoreFileRequest $request): RedirectResponse
    {
        $userId = Auth::id();

        $file = $this->service->store($request->validated(), $userId);

        if (is_null($file)) {
            return redirect()->back()->withErrors([
                'file' => __('Failed to upload file.')
            ]);
        }

        return redirect()->route('files.show', [
            'file' => $file['id']
        ]);
    }

    /**
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function show(int $id)
    {
        $file = $this->service->show($id);

        return view('pages.files.show')->with(compact('file'));
    }

    public function destroy(int $id): RedirectResponse
    {
        $this->service->destroy($id);

        return redirect()->route('files.index')->with([
            'status' => __('Deleted successfully.')
        ]);
    }
}
