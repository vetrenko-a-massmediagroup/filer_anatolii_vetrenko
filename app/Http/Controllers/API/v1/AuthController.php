<?php

namespace App\Http\Controllers\API\v1;

use App\Models\User;
use Exception;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

class AuthController extends Controller
{
    /**
     * @OA\Post(path="/api/v1/login",
     *   tags={"Auth"},
     *   @OA\Parameter(name="email",
     *     in="query",
     *     required=false,
     *     @OA\Schema(type="string")
     *   ),
     *   @OA\Parameter(name="password",
     *     in="query",
     *     required=false,
     *     @OA\Schema(type="string", format="password")
     *   ),
     *   @OA\Response(
     *     response="200",
     *     description="OK"
     *   )
     * )
     */
    public function login(Request $request): JsonResponse
    {
        try {
            $input = $request->only('email', 'password');

            if (!Auth::attempt($input)) {
                return $this->sendError('Unauthorized', 401);
            }

            $user = User::where('email', $input['email'])->first();

            if (!Hash::check($input['password'], $user->password, [])) {
                return $this->sendError('Login failed');
            }

            $accessToken = $user->createToken('authToken')->plainTextToken;

            return $this->sendResponse(['accessToken' => $accessToken]);

        } catch (Exception $error) {
            return $this->sendError('Exception on login', 500);
        }
    }

    /**
     * @OA\Post(path="/api/v1/logout",
     *   tags={"Auth"},
     *   @OA\Response(
     *     response="200",
     *     description="OK"
     *   )
     * )
     */
    public function logout(Request $request)
    {
        $request->user()->currentAccessToken()->delete();
    }
}
