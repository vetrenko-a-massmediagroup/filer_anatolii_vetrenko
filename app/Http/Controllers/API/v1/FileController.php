<?php

namespace App\Http\Controllers\API\v1;

use App\Http\Requests\StoreFileRequest;
use App\Models\File;
use App\Repositories\Contracts\FileRepository;
use App\Services\FileService;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;

class FileController extends Controller
{
    private FileService $service;
    private FileRepository $repository;

    public function __construct(FileService $service, FileRepository $repository)
    {
        $this->service = $service;
        $this->repository = $repository;
    }

    /**
     * @OA\get(path="/api/v1/files",
     *   tags={"Files"},
     *   summary="Show user files",
     *   @OA\Response(
     *     response="200",
     *     description="OK"
     *   )
     * )
     */
    public function index(): JsonResponse
    {
        $files = $this->repository->latest();
        $filesCount = count($files);

        return $this->sendResponse(compact('files', 'filesCount'));
    }

    /**
     * @OA\Post(path="/api/v1/files",
     *   tags={"Files"},
     *   summary="Store file",
     *   operationId="getFileById",
     *   @OA\Parameter(name="file",
     *     in="query",
     *     required=true,
     *     @OA\Schema(type="file")
     *   ),
     *   @OA\Parameter(name="comment",
     *     in="query",
     *     required=false,
     *     @OA\Schema(type="string")
     *   ),
     *   @OA\Parameter(name="delete_at",
     *     in="query",
     *     required=false,
     *     @OA\Schema(type="string", format="date")
     *   ),
     *   @OA\Response(
     *     response="200",
     *     description="OK",
     *   @OA\JsonContent(
     *              type="object",
     *              @OA\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @OA\Property(
     *                  property="data",
     *                  type="object"
     *              ),
     *              @OA\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *   )
     *   )
     * )
     */
    public function store(StoreFileRequest $request): JsonResponse
    {
        $userId = Auth::id();

        $file = $this->service->store($request->validated(), $userId);

        if (is_null($file)) {
            return $this->sendError(__('Upload failed.'));
        }

        return $this->sendResponse($file, __('Uploaded.'));
    }

    /**
     * @OA\Get(path="/api/v1/files/{id}",
     *   tags={"Files"},
     *   summary="Show file by id",
     *   operationId="getFileById",
     *   @OA\Parameter(name="id",
     *     in="path",
     *     required=true,
     *     @OA\Schema(type="string")
     *   ),
     *   @OA\Response(
     *     response="200",
     *     description="OK",
     *   @OA\JsonContent(
     *              type="object",
     *              @OA\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @OA\Property(
     *                  property="data",
     *                  type="object"
     *              ),
     *              @OA\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *   )
     *   )
     * )
     */
    public function show(int $id): JsonResponse
    {
        $file = $this->service->show($id);

        return $this->sendResponse($file);
    }

    /**
     * @OA\Delete (path="/api/v1/files/{id}",
     *   tags={"Files"},
     *   summary="Delete file by id",
     *   operationId="deleteFileById",
     *   @OA\Parameter(name="id",
     *     in="path",
     *     required=true,
     *     @OA\Schema(type="integer")
     *   ),
     *   @OA\Response(
     *     response="200",
     *     description="OK",
     *   @OA\JsonContent(
     *              type="object",
     *              @OA\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @OA\Property(
     *                  property="data",
     *                  type="object"
     *              ),
     *              @OA\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *    )
     *   ),
     *   security={
     *     "api_key":{}
     *   }
     * )
     */
    public function destroy(int $id): JsonResponse
    {
        $this->repository->delete($id);

        return $this->sendResponse(null, __('Deleted successfully'));
    }

    /**
     * @param $id
     * @return mixed
     *
     * @OA\Get (path="/api/v1/files/{id}/download",
     *   tags={"Files"},
     *   summary="Download file by id",
     *   operationId="downloadFileById",
     *   @OA\Parameter(name="id",
     *     in="path",
     *     required=true,
     *     @OA\Schema(type="integer")
     *   ),
     *   @OA\Response(
     *     response="200",
     *     description="OK",
     *   )
     *   )
     * )
     */
    public function download(int $id)
    {
        $file = $this->repository->find($id);

        return Storage::download($file['path']);
    }
}
