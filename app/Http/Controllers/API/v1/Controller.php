<?php
/** @OA\Info(
 *     title="Filer app",
 *     version="1"
 * )
 *
 * @OA\SecurityScheme(
 *      securityScheme="bearerAuth",
 *      in="header",
 *      name="bearerAuth",
 *      type="http",
 *      scheme="bearer",
 * )
 *
 *
 * @OA\SecurityScheme(
 *     type="apiKey",
 *     in="header",
 *     securityScheme="api_key",
 *     name="api_key"
 * )
 *
 */

namespace App\Http\Controllers\API\v1;

use Illuminate\Http\JsonResponse;

class Controller extends \App\Http\Controllers\Controller
{
    /**
     * @param $data
     * @param  string  $message
     * @param  int  $code
     * @return JsonResponse
     *
     */
    public function sendResponse($data, string $message = '', int $code = 200): JsonResponse
    {
        $response = [
            'success' => true,
            'data' => $data
        ];

        if (!empty($message)) {
            $response['message'] = $message;
        }

        return response()->json($response, $code);
    }

    /**
     * @param string|array $error
     * @param  int  $code
     * @return JsonResponse
     */
    public function sendError($error, $code = 404): JsonResponse
    {
        $response = [
            'success' => false
        ];

        if (is_array($error)) {
            $response['data'] = $error;
        } else {
            $response['message'] = $error;
        }

        return response()->json($response, $code);
    }
}
