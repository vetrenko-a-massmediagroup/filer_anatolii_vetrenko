<?php

namespace App\Http\Controllers\API\v1;

use App\Http\Requests\StoreShareTokenRequest;
use App\Services\ShareService;
use Illuminate\Http\JsonResponse;

class ShareController extends Controller
{
    /**
     * @var ShareService
     */
    private ShareService $shareService;

    public function __construct(ShareService $shareService)
    {
        $this->shareService = $shareService;
    }

    /**
     * @param  StoreShareTokenRequest  $request
     *
     * @OA\Post(path="/api/v1/share",
     *   tags={"Share"},
     *   summary="Generate share token",
     *   description="Type 0 = unlimited view count, 1 = view count limited to 1 non unique",
     *   @OA\Parameter(name="file_id",
     *     in="query",
     *     required=true,
     *     @OA\Schema(type="integer")
     *   ),
     *   @OA\Parameter(name="type",
     *     in="query",
     *     required=false,
     *     @OA\Schema(
     *      type="integer",
     *      default="0",
     *     )
     *   ),
     *   @OA\Response(
     *     response="200",
     *     description="OK",
     *   @OA\JsonContent(
     *              type="object",
     *              @OA\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @OA\Property(
     *                  property="data"
     *              ),
     *              @OA\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *   )
     * )
     */
    public function store(StoreShareTokenRequest $request): JsonResponse
    {
        $shareToken = $this->shareService->store($request->validated());

        return $this->sendResponse($shareToken);
    }
}
