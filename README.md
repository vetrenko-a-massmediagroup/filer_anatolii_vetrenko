# Setup your .env files
### Docker
Copy and setup `_docker/.env.example` to `_docker/.env`

### Project
Copy and setup `./.env.example` to `./.env` 

Set `DB_HOST` parameter as DBs container name

By default it `filer_database`, you can change it in `docker-compose.yaml`

`DB_PASSWORD`, `DB_USERNAME` and `DB_DATABASE` you can find and change in `_docker/.env`

Probably you need to generate app key and add 
`127.0.0.1 {{localSiteAddress}}` to `hosts` file (and check nginx configuration in _docker folder).

# Run

To start project you should run from `_docker` directory 

`docker-compose up`

And make seed and migration by running

`docker-compose exec php-fpm php artisan migrate --seed`